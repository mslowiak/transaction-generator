package com.uj.w6.transactiongenerator.model;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class TransactionTest {
    @Test
    public void noArgConstructor() {
        Transaction transaction = new Transaction();
        Assert.assertNotNull(transaction);
    }
    @Test
    public void argConstructor() {
        Transaction transaction = new Transaction(1, "", 1, null, new BigDecimal(0));
        Assert.assertNotNull(transaction);
    }
}
