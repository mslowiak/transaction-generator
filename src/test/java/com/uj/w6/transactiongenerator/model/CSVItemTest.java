package com.uj.w6.transactiongenerator.model;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class CSVItemTest {
    @Test
    public void argConstructor() {
        CSVItem csvItem = new CSVItem("name", new BigDecimal(0));
        Assert.assertNotNull(csvItem);
    }
}
