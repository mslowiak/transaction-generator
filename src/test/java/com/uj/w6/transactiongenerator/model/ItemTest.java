package com.uj.w6.transactiongenerator.model;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class ItemTest {
    @Test
    public void noArgConstructor() {
        Item item = new Item();
        Assert.assertNotNull(item);
    }
    @Test
    public void argConstructor() {
        Item item = new Item("name", 1 , new BigDecimal(0));
        Assert.assertNotNull(item);
    }
}
