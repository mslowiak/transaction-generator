package com.uj.w6.transactiongenerator;


import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeParseException;
import java.util.HashMap;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ParametersValidatorTest {
    private ParametersValidator parametersValidator = new ParametersValidator();

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void checkValidCustomerIds() {
        String attribute = "1:5";
        assertTrue(parametersValidator.validateCustomerIds(attribute));
    }

    @Test
    public void checkInvalidCustomerIdsBadSeparator() {
        String attribute = "1,5";
        assertFalse(parametersValidator.validateCustomerIds(attribute));
    }

    @Test(expected = NumberFormatException.class)
    public void checkInvalidCustomerIdsParseException() {
        String attribute = "1000000000000000000000000000000000000000000000000000000000000000000000001:5";
        assertTrue(parametersValidator.validateCustomerIds(attribute));
    }

    @Test
    public void checkInvalidCustomerIdsLeftHigherThanRight() {
        String attribute = "5:1";
        assertFalse(parametersValidator.validateCustomerIds(attribute));
    }

    @Test
    public void checkInvalidCustomerIdsFirstNonNumeric() {
        String attribute = "a:1";
        assertFalse(parametersValidator.validateCustomerIds(attribute));
    }

    @Test
    public void checkInvalidCustomerIdsSecondNonNumeric() {
        String attribute = "5:b";
        assertFalse(parametersValidator.validateCustomerIds(attribute));
    }

    @Test
    public void checkInvalidCustomerIdsBothNonNumeric() {
        String attribute = "a:b";
        assertFalse(parametersValidator.validateCustomerIds(attribute));
    }

    @Test
    public void checkValidDateRange() {
        String attribute = "2018-03-08T00:00:00.000-0100:2018-03-08T23:59:59.999-0100";
        assertTrue(parametersValidator.validateDateRange(attribute));
    }

    @Test
    public void checkInvalidDateRangeTooShort() {
        String attribute = "2018-03-08T00:00:00.000-0100:2018-03-08T23:59:59.999-010";
        assertFalse(parametersValidator.validateDateRange(attribute));
    }

    @Test
    public void checkInvalidDateRangeNoSeparator() {
        String attribute = "2018-03-08T00:00:00.000-0100^2018-03-08T23:59:59.999-0100";
        assertFalse(parametersValidator.validateDateRange(attribute));
    }

    @Test
    public void checkInvalidDateRangeTooShortAndNoSeparator() {
        String attribute = "2018-03-08T00:00:00.000-0100^2018-03-08T23:59:59.999-010";
        assertFalse(parametersValidator.validateDateRange(attribute));
    }

    @Test(expected = DateTimeParseException.class)
    public void checkInvalidDateRangeParseException() {
        String attribute = "2018-03-08T00:00:00.000-0A00:2018-03-08T23:59:59.999-0100";
        assertFalse(parametersValidator.validateDateRange(attribute));
    }

    @Test
    public void checkValidItemsCount() {
        String attribute = "1:5";
        assertTrue(parametersValidator.validateItemsCount(attribute));
    }

    @Test
    public void checkInvalidItemsCountBadSeparator() {
        String attribute = "1,5";
        assertFalse(parametersValidator.validateItemsCount(attribute));
    }

    @Test(expected = NumberFormatException.class)
    public void checkInvalidItemsCountParseException() {
        String attribute = "1000000000000000000000000000000000000000000000000000000000000000000000001:5";
        assertTrue(parametersValidator.validateItemsCount(attribute));
    }

    @Test
    public void checkInvalidItemsCountLeftHigherThanRight() {
        String attribute = "5:1";
        assertFalse(parametersValidator.validateItemsCount(attribute));
    }

    @Test
    public void checkInvalidItemsCountFirstNonNumeric() {
        String attribute = "a:1";
        assertFalse(parametersValidator.validateItemsCount(attribute));
    }

    @Test
    public void checkInvalidItemsCountSecondNonNumeric() {
        String attribute = "5:b";
        assertFalse(parametersValidator.validateItemsCount(attribute));
    }

    @Test
    public void checkInvalidItemsCountBothNonNumeric() {
        String attribute = "a:b";
        assertFalse(parametersValidator.validateItemsCount(attribute));
    }

    @Test
    public void checkValidItemsQuantity() {
        String attribute = "1:5";
        assertTrue(parametersValidator.validateItemsQuantity(attribute));
    }

    @Test
    public void checkInvalidItemsQuantityBadSeparator() {
        String attribute = "1,5";
        assertFalse(parametersValidator.validateItemsQuantity(attribute));
    }

    @Test(expected = NumberFormatException.class)
    public void checkInvalidItemsQuantityParseException() {
        String attribute = "1000000000000000000000000000000000000000000000000000000000000000000000001:5";
        assertTrue(parametersValidator.validateItemsQuantity(attribute));
    }

    @Test
    public void checkInvalidItemsQuantityLeftHigherThanRight() {
        String attribute = "5:1";
        assertFalse(parametersValidator.validateItemsQuantity(attribute));
    }

    @Test
    public void checkInvalidItemsQuantityFirstNonNumeric() {
        String attribute = "a:1";
        assertFalse(parametersValidator.validateItemsQuantity(attribute));
    }

    @Test
    public void checkInvalidItemsQuantitySecondNonNumeric() {
        String attribute = "5:b";
        assertFalse(parametersValidator.validateItemsQuantity(attribute));
    }

    @Test
    public void checkInvalidItemsQuantityBothNonNumeric() {
        String attribute = "a:b";
        assertFalse(parametersValidator.validateItemsQuantity(attribute));
    }

    @Test
    public void checkValidEventsCount() {
        String attribute = "10";
        assertTrue(parametersValidator.validateEventsCount(attribute));
    }

    @Test
    public void checkInvalidEventsCountNonNumeric() {
        String attribute = "a";
        assertFalse(parametersValidator.validateEventsCount(attribute));
    }

    @Test(expected = NumberFormatException.class)
    public void checkInvalidEventsCountParseException() {
        String attribute = "10000000000000000000000000000000000000000";
        assertFalse(parametersValidator.validateEventsCount(attribute));
    }

    @Test
    public void checkAllParametersWithOneInvalid() throws IOException {
        File dir = temporaryFolder.newFolder();
        File file = temporaryFolder.newFile();
        HashMap<String, String> map = new HashMap<>();
        map.put("customerIds", "1:5");
        map.put("dateRange", "dasdas");
        map.put("itemsFile", file.getAbsolutePath());
        map.put("itemsCount", "1:5");
        map.put("itemsQuantity", "1:5");
        map.put("eventsCount", "5");
        map.put("outDir", dir.getAbsolutePath());
        assertFalse(parametersValidator.validate(map));
    }

    @Test
    public void checkAllParametersWithAllValid() throws IOException {
        File dir = temporaryFolder.newFolder();
        File file = temporaryFolder.newFile();
        HashMap<String, String> map = new HashMap<>();
        map.put("customerIds", "1:5");
        map.put("dateRange", "2018-03-08T00:00:00.000-0100:2018-03-08T23:59:59.999-0100");
        map.put("itemsFile", file.getAbsolutePath());
        map.put("itemsCount", "1:5");
        map.put("itemsQuantity", "1:5");
        map.put("eventsCount", "5");
        map.put("outDir", dir.getAbsolutePath());
        assertTrue(parametersValidator.validate(map));
    }
}