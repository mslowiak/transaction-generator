package com.uj.w6.transactiongenerator.randomizer;

import com.uj.w6.transactiongenerator.model.Transaction;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.math.BigDecimal;
import java.time.format.DateTimeParseException;

public class TransactionRandomizerTest {
    @Mock
    private ItemRandomizer itemRandomizer;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void correctDataTest() {
        Mockito.when(itemRandomizer.randomize()).thenReturn(null);
        Mockito.when(itemRandomizer.getTotalSumOfItems()).thenReturn(new BigDecimal(0));
        TransactionRandomizer transactionRandomizer = new TransactionRandomizer(
                1,
                2,
                10,
                "2018-03-08T00:00:00.000-0100",
                "2018-03-08T23:59:59.000-0100");

        Transaction transaction = transactionRandomizer.randomize(itemRandomizer.randomize(), itemRandomizer.getTotalSumOfItems());

        Assert.assertEquals(transaction.getId(), 1);
        Assert.assertTrue(2 <= transaction.getCustomer_id() && transaction.getCustomer_id() <= 10);
        Assert.assertTrue(transaction.getTimestamp().startsWith("2018-03-08"));
        Assert.assertEquals(transaction.getItems(), null);
        Assert.assertThat(transaction.getSum(), Matchers.comparesEqualTo(new BigDecimal(0)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrectCustomerIdRange() {
        Mockito.when(itemRandomizer.randomize()).thenReturn(null);
        Mockito.when(itemRandomizer.getTotalSumOfItems()).thenReturn(new BigDecimal(0));
        TransactionRandomizer transactionRandomizer = new TransactionRandomizer(
                1,
                5,
                2,
                "2018-03-08T00:00:00.000-0100",
                "2018-03-08T23:59:59.000-0100");

        transactionRandomizer.randomize(itemRandomizer.randomize(), itemRandomizer.getTotalSumOfItems());
    }

    @Test
    public void singleCustomerIdRange() {
        Mockito.when(itemRandomizer.randomize()).thenReturn(null);
        Mockito.when(itemRandomizer.getTotalSumOfItems()).thenReturn(new BigDecimal(0));
        TransactionRandomizer transactionRandomizer = new TransactionRandomizer(
                1,
                5,
                5,
                "2018-03-08T00:00:00.000-0100",
                "2018-03-08T23:59:59.000-0100");

        Transaction transaction = transactionRandomizer.randomize(itemRandomizer.randomize(), itemRandomizer.getTotalSumOfItems());

        Assert.assertEquals(transaction.getCustomer_id(), 5);
    }

    @Test(expected = DateTimeParseException.class)
    public void incorrectTimestampFormat() {
        Mockito.when(itemRandomizer.randomize()).thenReturn(null);
        Mockito.when(itemRandomizer.getTotalSumOfItems()).thenReturn(new BigDecimal(0));
        TransactionRandomizer transactionRandomizer = new TransactionRandomizer(
                1,
                1,
                5,
                "2018-03-08",
                "2018-03-09");

        transactionRandomizer.randomize(itemRandomizer.randomize(), itemRandomizer.getTotalSumOfItems());
    }

    @Test
    public void correctIdIncrementation() {
        Mockito.when(itemRandomizer.randomize()).thenReturn(null);
        Mockito.when(itemRandomizer.getTotalSumOfItems()).thenReturn(new BigDecimal(0));
        TransactionRandomizer transactionRandomizer = new TransactionRandomizer(
                1,
                1,
                5,
                "2018-03-08T00:00:00.000-0100",
                "2018-03-08T23:59:59.000-0100");

        Transaction firstRandomizedTransaction = transactionRandomizer.randomize(itemRandomizer.randomize(), itemRandomizer.getTotalSumOfItems());
        Transaction secondRandomizedTransaction = transactionRandomizer.randomize(itemRandomizer.randomize(), itemRandomizer.getTotalSumOfItems());

        Assert.assertEquals(firstRandomizedTransaction.getId(), 1);
        Assert.assertEquals(secondRandomizedTransaction.getId(), 2);
    }
}
