package com.uj.w6.transactiongenerator.randomizer;

import com.uj.w6.transactiongenerator.model.CSVItem;
import com.uj.w6.transactiongenerator.model.Item;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class ItemRandomizerTest {
    private List<String> itemList;
    private List<CSVItem> csvItemsList;

    @Before
    public void setUp() {
        itemList = Arrays.asList("\"mleko 3% 1l\",2.30",
                "\"bułeczka\",1.20",
                "\"chleb biały\",2.20",
                "\"chleb ciemny\",2.50",
                "\"masło\",5.0",
                "\"szynka 1kg\",30.0",
                "\"jajko\",0.50",
                "\"woda gazowana 1.5l\",2.10",
                "\"woda 1.5l\",2.00");

        csvItemsList = Arrays.asList(
                new CSVItem("mleko 3% 1l", new BigDecimal(2.30)),
                new CSVItem("bułeczka", new BigDecimal(1.20)),
                new CSVItem("chleb biały", new BigDecimal(2.20)),
                new CSVItem("chleb ciemny", new BigDecimal(2.50)),
                new CSVItem("masło", new BigDecimal(5.0)),
                new CSVItem("szynka 1kg", new BigDecimal(30.0)),
                new CSVItem("jajko", new BigDecimal(0.50)),
                new CSVItem("woda gazowana 1.5l", new BigDecimal(2.10)),
                new CSVItem("woda 1.5l", new BigDecimal(2.00))
        );

    }

    @Test
    public void correctInput() {
        ItemRandomizer itemRandomizer = new ItemRandomizer(
                csvItemsList,
                3,
                4,
                3,
                6);

        List<Item> randomizedItems = itemRandomizer.randomize();
        Item i = randomizedItems.get(0);

        Assert.assertTrue(3 <= randomizedItems.size() && randomizedItems.size() <= 6);
        Assert.assertTrue(3 <= i.getQuantity() && i.getQuantity() <= 4);
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrectItemQuantityRange() {
        ItemRandomizer itemRandomizer = new ItemRandomizer(
                csvItemsList,
                5,
                3,
                3,
                6);

        itemRandomizer.randomize();
    }

    @Test
    public void singleItemQuantityRange() {
        ItemRandomizer itemRandomizer = new ItemRandomizer(
                csvItemsList,
                3,
                3,
                3,
                6);

        List<Item> randomizedItems = itemRandomizer.randomize();
        Assert.assertEquals(randomizedItems.get(0).getQuantity(), 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrectItemsCountRange() {
        ItemRandomizer itemRandomizer = new ItemRandomizer(
                csvItemsList,
                3,
                4,
                6,
                3);

        itemRandomizer.randomize();
    }

    @Test
    public void singleItemsCountRange() {
        ItemRandomizer itemRandomizer = new ItemRandomizer(
                csvItemsList,
                3,
                4,
                3,
                3);

        List<Item> randomizedItems = itemRandomizer.randomize();
        Assert.assertEquals(randomizedItems.size(), 3);
    }

    @Test
    public void singleItemRandomization() {
        List<CSVItem> items = Collections.singletonList(new CSVItem("masło", new BigDecimal(5)));
        ItemRandomizer itemRandomizer = new ItemRandomizer(
                items,
                2,
                2,
                1,
                1);
        List<Item> randomizedItems = itemRandomizer.randomize();

        Assert.assertEquals(randomizedItems.size(), 1);
        Assert.assertEquals(randomizedItems.get(0).getQuantity(), 2);
        Assert.assertEquals(randomizedItems.get(0).getName(), "masło");
        Assert.assertThat(randomizedItems.get(0).getPrice(), Matchers.comparesEqualTo(new BigDecimal(5)));
        Assert.assertThat(itemRandomizer.getTotalSumOfItems(), Matchers.comparesEqualTo(new BigDecimal(10)));
    }


    @Test(expected = NullPointerException.class)
    public void emptyList() {
        ItemRandomizer itemRandomizer = new ItemRandomizer(
                null,
                2,
                2,
                1,
                1);
        itemRandomizer.randomize();
    }
}
