package com.uj.w6.transactiongenerator;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class RequestParametersParserTest {

    @Test
    public void allParametersGiven(){
        // Given
        Map<String, String> mapWithAttributes = new HashMap<>();
        mapWithAttributes.put("customerIds", "1:20");
        mapWithAttributes.put("itemsCount", "1:5");
        mapWithAttributes.put("itemsQuantity", "1:5");
        mapWithAttributes.put("eventsCount", "100");
        mapWithAttributes.put("dateRange", "2018-05-31T00:00:00.000+0200:2018-05-31T23:59:59.999+0200");
        RequestParametersParser requestParametersParser = new RequestParametersParser();
        requestParametersParser.setInputParams(mapWithAttributes);

        // When
        Map<String, String> parsedParameters = requestParametersParser.parse();

        // Then
        Assert.assertEquals(parsedParameters, mapWithAttributes);
    }

    @Test
    public void noParametersSet(){
        // Given
        Map<String, String> mapWithAttributes = new HashMap<>();
        RequestParametersParser requestParametersParser = new RequestParametersParser();
        requestParametersParser.setInputParams(mapWithAttributes);

        // When
        requestParametersParser.setInputParams(mapWithAttributes);
        Map<String, String> parsedParameters = requestParametersParser.parse();

        // Then
        Assert.assertTrue(parsedParameters.size() == 5);
    }

    @Test
    public void checkDefaultCustomerIdsParameter(){
        // Given
        Map<String, String> mapWithAttributes = new HashMap<>();
        RequestParametersParser requestParametersParser = new RequestParametersParser();
        requestParametersParser.setInputParams(mapWithAttributes);

        // When
        requestParametersParser.setInputParams(mapWithAttributes);
        Map<String, String> parsedParameters = requestParametersParser.parse();

        // Then
        Assert.assertTrue(parsedParameters.get("customerIds").equals("1:20"));
    }

    @Test
    public void checkDefaultItemsCountParameter(){
        // Given
        Map<String, String> mapWithAttributes = new HashMap<>();
        RequestParametersParser requestParametersParser = new RequestParametersParser();
        requestParametersParser.setInputParams(mapWithAttributes);

        // When
        requestParametersParser.setInputParams(mapWithAttributes);
        Map<String, String> parsedParameters = requestParametersParser.parse();

        // Then
        Assert.assertTrue(parsedParameters.get("itemsCount").equals("1:5"));
    }

    @Test
    public void checkDefaultItemsQuantityParameter(){
        // Given
        Map<String, String> mapWithAttributes = new HashMap<>();
        RequestParametersParser requestParametersParser = new RequestParametersParser();
        requestParametersParser.setInputParams(mapWithAttributes);

        // When
        requestParametersParser.setInputParams(mapWithAttributes);
        Map<String, String> parsedParameters = requestParametersParser.parse();

        // Then
        Assert.assertTrue(parsedParameters.get("itemsQuantity").equals("1:5"));
    }

    @Test
    public void checkDefaultEventsCountParameter(){
        // Given
        Map<String, String> mapWithAttributes = new HashMap<>();
        RequestParametersParser requestParametersParser = new RequestParametersParser();
        requestParametersParser.setInputParams(mapWithAttributes);

        // When
        requestParametersParser.setInputParams(mapWithAttributes);
        Map<String, String> parsedParameters = requestParametersParser.parse();

        // Then
        Assert.assertTrue(parsedParameters.get("eventsCount").equals("100"));
    }
}
