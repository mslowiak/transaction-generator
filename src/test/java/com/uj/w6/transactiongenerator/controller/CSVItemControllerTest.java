package com.uj.w6.transactiongenerator.controller;

import com.uj.w6.transactiongenerator.service.CSVItemService;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

public class CSVItemControllerTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private CSVItemService csvItemService;

    @Test
    public void checkItemsDownloading() {
        CSVItemController csvItemController = new CSVItemController(csvItemService);
        csvItemController.getItems();
        Mockito.verify(csvItemService, Mockito.times(1)).getCSVItems();
    }
}
