package com.uj.w6.transactiongenerator.controller;

import com.uj.w6.transactiongenerator.service.TransactionService;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;

public class TransactionControllerTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private TransactionService transactionService;

    @Test
    public void checkTransactionsXml() {
        // Given
        TransactionController transactionController = new TransactionController(transactionService);
        Mockito.when(transactionService.getTransactions(Mockito.anyMap())).thenReturn(null);

        // When
        transactionController.getTransactionsXml("", "", "", "", "");

        // Then
        Mockito.verify(transactionService, Mockito.times(1)).getTransactions(Mockito.anyMap());
    }

    @Test
    public void checkTransactionsJSON() {
        // Given
        TransactionController transactionController = new TransactionController(transactionService);
        Mockito.when(transactionService.getTransactions(Mockito.anyMap())).thenReturn(null);

        // When
        transactionController.getTransactionsJson("", "", "", "", "");

        // Then
        Mockito.verify(transactionService, Mockito.times(1)).getTransactions(Mockito.anyMap());
    }

    @Test
    public void checkTransactionsYml() {
        // Given
        TransactionController transactionController = new TransactionController(transactionService);
        Mockito.when(transactionService.getTransactions(Mockito.anyMap())).thenReturn(new ArrayList<>());

        // When
        transactionController.getTransactionsAsYaml("", "", "", "", "");

        // Then
        Mockito.verify(transactionService, Mockito.times(1)).getTransactions(Mockito.anyMap());
    }
}
