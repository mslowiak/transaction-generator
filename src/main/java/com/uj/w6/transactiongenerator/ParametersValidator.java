package com.uj.w6.transactiongenerator;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.stream.Stream;

@Component
@Log4j2
public class ParametersValidator {

    public boolean validate(Map<String, String> mapOfValues) {
        return Stream.of(validateCustomerIds(mapOfValues.get("customerIds")),
                validateDateRange(mapOfValues.get("dateRange")),
                validateItemsCount(mapOfValues.get("itemsCount")),
                validateItemsQuantity(mapOfValues.get("itemsQuantity")),
                validateEventsCount(mapOfValues.get("eventsCount")))
                .allMatch(val -> val);
    }

    boolean validateCustomerIds(String attribute) {
        if (!attribute.contains(":")) {
            log.warn("validateCustomerIds failed - not contains ':'");
            return false;
        } else {
            String[] numbers = attribute.split(":");
            if (!StringUtils.isNumeric(numbers[0]) || !StringUtils.isNumeric(numbers[1])) {
                log.warn("validateItemsCount failed - not numeric");
                return false;
            }
            int numStart = Integer.parseInt(numbers[0]);
            int numStop = Integer.parseInt(numbers[1]);
            if (numStart > numStop) {
                log.warn("validateCustomerIds failed - num1>num2");
                return false;
            }
        }
        log.info("validateCustomerIds passed");
        return true;
    }

    boolean validateDateRange(String attribute) {
        if (attribute.length() != 57 || attribute.charAt(28) != ':') {
            log.warn("validateDateRange failed - invalid length and no ':' at pos 28");
            return false;
        } else {
            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            String firstDate = attribute.substring(0, 28);
            String secondDate = attribute.substring(29);
            ZonedDateTime.parse(firstDate, dateFormat);
            ZonedDateTime.parse(secondDate, dateFormat);
        }
        log.info("validateDateRange passed");
        return true;
    }

    boolean validateItemsCount(String attribute) {
        if (!attribute.contains(":")) {
            log.warn("validateItemsCount failed - not contains ':'");
            return false;
        } else {
            String[] numbers = attribute.split(":");
            if (!StringUtils.isNumeric(numbers[0]) || !StringUtils.isNumeric(numbers[1])) {
                log.warn("validateItemsCount failed - not numeric");
                return false;
            }
            int numStart = Integer.parseInt(numbers[0]);
            int numStop = Integer.parseInt(numbers[1]);
            if (numStart > numStop) {
                log.warn("validateItemsCount failed - num1>num2");
                return false;
            }
        }
        log.info("validateItemsCount passed");
        return true;
    }

    boolean validateItemsQuantity(String attribute) {
        if (!attribute.contains(":")) {
            log.warn("validateItemsQuantity failed - not contains ':'");
            return false;
        } else {
            String[] numbers = attribute.split(":");
            if (!StringUtils.isNumeric(numbers[0]) || !StringUtils.isNumeric(numbers[1])) {
                log.warn("validateItemsQuantity failed - not numeric");
                return false;
            }
            int numStart = Integer.parseInt(numbers[0]);
            int numStop = Integer.parseInt(numbers[1]);
            if (numStart > numStop) {
                log.warn("validateItemsQuantity failed - num1>num2");
                return false;
            }
        }
        log.info("validateItemsQuantity passed");
        return true;
    }

    boolean validateEventsCount(String attribute) {
        if (!StringUtils.isNumeric(attribute)) {
            log.warn("validateEventsCount - not numeric");
            return false;
        }
        Integer.parseInt(attribute);
        return true;
    }

}
