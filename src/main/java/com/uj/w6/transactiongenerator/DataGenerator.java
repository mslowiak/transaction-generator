package com.uj.w6.transactiongenerator;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.uj.w6.transactiongenerator.model.CSVItem;
import com.uj.w6.transactiongenerator.model.Transaction;
import com.uj.w6.transactiongenerator.randomizer.ItemRandomizer;
import com.uj.w6.transactiongenerator.randomizer.TransactionRandomizer;
import com.uj.w6.transactiongenerator.service.CSVItemService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Log4j2
public class DataGenerator {
    private RequestParametersParser requestParametersParser;
    private ParametersValidator parametersValidator;
    private CSVItemService csvItemService;

    private TransactionRandomizer transactionRandomizer;
    private ItemRandomizer itemRandomizer;


    public DataGenerator(RequestParametersParser requestParametersParser, ParametersValidator parametersValidator, CSVItemService csvItemService) {
        this.requestParametersParser = requestParametersParser;
        this.parametersValidator = parametersValidator;
        this.csvItemService = csvItemService;
    }

    public List<Transaction> generateTransactions(Map<String, String> mapOfAttributes) {
        requestParametersParser.setInputParams(mapOfAttributes);
        mapOfAttributes = requestParametersParser.parse();
        if (parametersValidator.validate(mapOfAttributes)) {
            assignAttributesToRandomizers(mapOfAttributes);
            return randomizeTransactions(mapOfAttributes);
        } else {
            log.error("Validation failed!");
        }
        return null;
    }

    void assignAttributesToRandomizers(Map<String, String> mapOfValues) {
        List<CSVItem> itemsFile = csvItemService.getCSVItems();
        List<Integer> itemsQuantity = Arrays.stream(mapOfValues.get("itemsQuantity").split(":"))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        List<Integer> itemsCount = Arrays.stream(mapOfValues.get("itemsCount").split(":"))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        List<Integer> customerIds = Arrays.stream(mapOfValues.get("customerIds").split(":"))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        String startDate = mapOfValues.get("dateRange").substring(0, 28);
        String endDate = mapOfValues.get("dateRange").substring(29);

        itemRandomizer = new ItemRandomizer(
                itemsFile,
                itemsQuantity.get(0),
                itemsQuantity.get(1),
                itemsCount.get(0),
                itemsCount.get(1)
        );

        log.info("ItemRandomizer is set");
        log.debug(itemRandomizer);

        transactionRandomizer = new TransactionRandomizer(
                1,
                customerIds.get(0),
                customerIds.get(1),
                startDate,
                endDate
        );
        log.info("TransactionRandomizer is set");
        log.debug(transactionRandomizer);
    }

    @JacksonXmlElementWrapper(localName = "transaction")
    List<Transaction> randomizeTransactions(Map<String, String> mapOfValues) {
        List<Transaction> randomizedTransactions = new ArrayList<>();
        int numberOfTransactions = Integer.parseInt(mapOfValues.get("eventsCount"));
        for (int i = 0; i < numberOfTransactions; ++i) {
            Transaction randomizedTransaction = transactionRandomizer.randomize(
                    itemRandomizer.randomize(),
                    itemRandomizer.getTotalSumOfItems());
            randomizedTransactions.add(randomizedTransaction);
            log.debug("Generated: " + randomizedTransaction);
        }
        log.info("Transactions randomization complete");
        return randomizedTransactions;
    }
}
