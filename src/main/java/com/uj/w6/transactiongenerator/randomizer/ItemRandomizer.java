package com.uj.w6.transactiongenerator.randomizer;

import com.uj.w6.transactiongenerator.model.CSVItem;
import com.uj.w6.transactiongenerator.model.Item;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@ToString
@Log4j2
public class ItemRandomizer {
    private List<CSVItem> itemDetailListFromCSV;
    private int startItemQuantity;
    private int stopItemQuantity;
    private int startItemsCount;
    private int stopItemsCount;
    private BigDecimal totalSumOfItems;

    public ItemRandomizer(List<CSVItem> itemsList, int startItemQuantity, int stopItemQuantity, int startItemsCount, int stopItemsCount) {
        this.itemDetailListFromCSV = itemsList;
        this.startItemQuantity = startItemQuantity;
        this.stopItemQuantity = stopItemQuantity;
        this.startItemsCount = startItemsCount;
        this.stopItemsCount = stopItemsCount;
    }

    public List<Item> randomize() {
        totalSumOfItems = new BigDecimal(0);
        List<Item> items = new ArrayList<>();
        int numberOfItemsToRandomize = ThreadLocalRandom.current().nextInt(startItemsCount, stopItemsCount + 1);
        for (int i = 0; i < numberOfItemsToRandomize; ++i) {
            int randomItemNumber = new Random().nextInt(itemDetailListFromCSV.size());
            String itemName = itemDetailListFromCSV.get(randomItemNumber).getName();
            BigDecimal itemPrice = itemDetailListFromCSV.get(randomItemNumber).getPrice();
            int itemQuantity = ThreadLocalRandom.current().nextInt(startItemQuantity, stopItemQuantity + 1);

            Item randomizedItem = new Item(itemName, itemQuantity, itemPrice);
            items.add(randomizedItem);
            totalSumOfItems = totalSumOfItems.add(itemPrice.multiply(new BigDecimal(itemQuantity)));
        }
        return items;
    }

    public BigDecimal getTotalSumOfItems() {
        return totalSumOfItems.multiply(new BigDecimal(100)).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
    }
}
