package com.uj.w6.transactiongenerator.randomizer;

import com.uj.w6.transactiongenerator.model.Item;
import com.uj.w6.transactiongenerator.model.Transaction;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class TransactionRandomizer {
    private int transactionId;
    private int startCustomerIdValue;
    private int stopCustomerIdValue;
    private String startTimestamp;
    private String stopTimestamp;

    public TransactionRandomizer(int transactionId, int startCustomerIdValue, int stopCustomerIdValue, String startTimestamp, String stopTimestamp) {
        this.transactionId = transactionId;
        this.startCustomerIdValue = startCustomerIdValue;
        this.stopCustomerIdValue = stopCustomerIdValue;
        this.startTimestamp = startTimestamp;
        this.stopTimestamp = stopTimestamp;
    }

    public Transaction randomize(List<Item> listOfItems, BigDecimal totalSumOfItems) {
        int randomCustomerId = ThreadLocalRandom.current().nextInt(startCustomerIdValue, stopCustomerIdValue + 1);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        ZonedDateTime zdt1 = ZonedDateTime.parse(startTimestamp, dateTimeFormatter);
        ZonedDateTime zdt2 = ZonedDateTime.parse(stopTimestamp, dateTimeFormatter);

        long time1 = Timestamp.from(zdt1.toInstant()).getTime();
        long time2 = Timestamp.from(zdt2.toInstant()).getTime();

        long randomTimestampInLong = time1 + (long) (Math.random() * (time2 - time1));
        Instant instant = Instant.ofEpochMilli(randomTimestampInLong);
        ZonedDateTime z = ZonedDateTime.ofInstant(instant, zdt1.getZone());

        Transaction transaction = new Transaction(
                transactionId,
                dateTimeFormatter.format(z),
                randomCustomerId,
                listOfItems,
                totalSumOfItems);

        transactionId++;
        return transaction;
    }
}
