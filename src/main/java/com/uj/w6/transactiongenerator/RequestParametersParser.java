package com.uj.w6.transactiongenerator;

import lombok.Setter;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Component
@Setter
public class RequestParametersParser {
    private Map<String, String> inputParams;

    public Map<String, String> parse() {
        Map<String, String> mapWithAttributes = new HashMap<>();

        String dateRange = inputParams.get("dateRange");
        String customerIds = ((inputParams.get("customerIds") == null) ? "1:20" : (inputParams.get("customerIds")));
        String itemsCount = ((inputParams.get("itemsCount") == null) ? "1:5" : (inputParams.get("itemsCount")));
        String itemsQuantity = ((inputParams.get("itemsQuantity") == null) ? "1:5" : (inputParams.get("itemsQuantity")));
        String eventsCount = ((inputParams.get("eventsCount") == null) ? "100" : (inputParams.get("eventsCount")));

        if (inputParams.get("dateRange") == null) {
            DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("Z");
            ZonedDateTime actualTime = ZonedDateTime.now();
            String date = actualTime.format(dtf1);
            String zone = actualTime.format(dtf2);
            dateRange = date + "T00:00:00.000" + zone + ":" + date + "T23:59:59.999" + zone;
        } else {
            dateRange = dateRange.replace("\"", "");
        }

        mapWithAttributes.put("customerIds", customerIds);
        mapWithAttributes.put("itemsCount", itemsCount);
        mapWithAttributes.put("itemsQuantity", itemsQuantity);
        mapWithAttributes.put("eventsCount", eventsCount);
        mapWithAttributes.put("dateRange", dateRange);

        return mapWithAttributes;
    }
}
