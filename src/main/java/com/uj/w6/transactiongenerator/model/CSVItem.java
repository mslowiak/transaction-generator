package com.uj.w6.transactiongenerator.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CSVItem {
    private final String name;
    private final BigDecimal price;

    @JsonCreator
    public CSVItem(
            @JsonProperty("name") String name,
            @JsonProperty("price") BigDecimal price) {
        this.name = name;
        this.price = price;
    }
}
