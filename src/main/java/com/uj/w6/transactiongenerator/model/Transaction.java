package com.uj.w6.transactiongenerator.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class Transaction {
    private int id;
    private String timestamp;
    private int customer_id;
    @JacksonXmlElementWrapper(localName = "items")
    @JacksonXmlProperty(localName = "item")
    private List<Item> items;
    private BigDecimal sum;

    public Transaction() {
    }

    public Transaction(int id, String timestamp, int customer_id, List<Item> items, BigDecimal sum) {
        this.id = id;
        this.timestamp = timestamp;
        this.customer_id = customer_id;
        this.items = items;
        this.sum = sum;
    }
}