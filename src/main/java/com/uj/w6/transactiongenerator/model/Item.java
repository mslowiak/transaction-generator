package com.uj.w6.transactiongenerator.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Item {
    private String name;
    private int quantity;
    private BigDecimal price;

    public Item() {
    }

    public Item(String name, int quantity, BigDecimal price) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }
}