package com.uj.w6.transactiongenerator.controller;

import com.uj.w6.transactiongenerator.model.Transaction;
import com.uj.w6.transactiongenerator.model.TransactionList;
import com.uj.w6.transactiongenerator.service.TransactionService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Log4j2
public class TransactionController {
    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = "application/xml")
    public TransactionList getTransactionsXml(
            @RequestParam(value = "customerIds", required = false) String customerIds,
            @RequestParam(value = "dateRange", required = false) String dateRange,
            @RequestParam(value = "itemsCount", required = false) String itemsCount,
            @RequestParam(value = "itemsQuantity", required = false) String itemsQuantity,
            @RequestParam(value = "eventsCount", required = false) String eventsCount
    ) {
        log.info("xml");
        Map<String, String> mapWithAttributes = new HashMap<>();
        mapWithAttributes.put("customerIds", customerIds);
        mapWithAttributes.put("dateRange", dateRange);
        mapWithAttributes.put("itemsCount", itemsCount);
        mapWithAttributes.put("itemsQuantity", itemsQuantity);
        mapWithAttributes.put("eventsCount", eventsCount);

        return new TransactionList(transactionService.getTransactions(mapWithAttributes));
    }

    @RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = "application/json")
    public List<Transaction> getTransactionsJson(
            @RequestParam(value = "customerIds", required = false) String customerIds,
            @RequestParam(value = "dateRange", required = false) String dateRange,
            @RequestParam(value = "itemsCount", required = false) String itemsCount,
            @RequestParam(value = "itemsQuantity", required = false) String itemsQuantity,
            @RequestParam(value = "eventsCount", required = false) String eventsCount
    ) {
        log.info("json");
        Map<String, String> mapWithAttributes = new HashMap<>();
        mapWithAttributes.put("customerIds", customerIds);
        mapWithAttributes.put("dateRange", dateRange);
        mapWithAttributes.put("itemsCount", itemsCount);
        mapWithAttributes.put("itemsQuantity", itemsQuantity);
        mapWithAttributes.put("eventsCount", eventsCount);

        return transactionService.getTransactions(mapWithAttributes);
    }

    @RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = {"application/yml"})
    public String getTransactionsAsYaml(
            @RequestParam(value = "customerIds", required = false) String customerIds,
            @RequestParam(value = "dateRange", required = false) String dateRange,
            @RequestParam(value = "itemsCount", required = false) String itemsCount,
            @RequestParam(value = "itemsQuantity", required = false) String itemsQuantity,
            @RequestParam(value = "eventsCount", required = false) String eventsCount
    ) {
        log.info("YAML");
        Map<String, String> mapWithAttributes = new HashMap<>();
        mapWithAttributes.put("customerIds", customerIds);
        mapWithAttributes.put("dateRange", dateRange);
        mapWithAttributes.put("itemsCount", itemsCount);
        mapWithAttributes.put("itemsQuantity", itemsQuantity);
        mapWithAttributes.put("eventsCount", eventsCount);

        List<Transaction> transactions = transactionService.getTransactions(mapWithAttributes);

        Representer representer = new Representer();
        representer.addClassTag(Transaction.class, new Tag("!transaction"));
        Yaml yaml = new Yaml(representer, new DumperOptions());
        Writer writer = new StringWriter();

        for(Transaction transaction : transactions){
            yaml.dump(transaction, writer);
        }
        return writer.toString();
    }
}
