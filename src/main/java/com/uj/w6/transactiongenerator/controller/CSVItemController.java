package com.uj.w6.transactiongenerator.controller;

import com.uj.w6.transactiongenerator.model.CSVItem;
import com.uj.w6.transactiongenerator.service.CSVItemService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CSVItemController {
    private final CSVItemService csvItemService;

    public CSVItemController(CSVItemService csvItemService) {
        this.csvItemService = csvItemService;
    }

    @RequestMapping(value = "/items", method = RequestMethod.GET, produces = {"application/json", "application/xml"})
    public List<CSVItem> getItems() {
        return csvItemService.getCSVItems();
    }
}
