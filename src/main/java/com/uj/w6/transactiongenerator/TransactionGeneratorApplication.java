package com.uj.w6.transactiongenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionGeneratorApplication.class, args);
	}
}
