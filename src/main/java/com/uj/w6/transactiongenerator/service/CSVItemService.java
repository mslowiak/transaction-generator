package com.uj.w6.transactiongenerator.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uj.w6.transactiongenerator.model.CSVItem;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

@Service
@Log4j2
public class CSVItemService {
    public CSVItemService() {
    }

    public List<CSVItem> getCSVItems() {
        List<CSVItem> itemsList = null;
        try {
            URL url = new URL("https://csv-items-generator.herokuapp.com/items");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();

            ObjectMapper mapper = new ObjectMapper();
            itemsList = mapper.readValue(content.toString(), new TypeReference<List<CSVItem>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return itemsList;
    }
}
