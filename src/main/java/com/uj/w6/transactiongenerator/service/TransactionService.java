package com.uj.w6.transactiongenerator.service;

import com.uj.w6.transactiongenerator.DataGenerator;
import com.uj.w6.transactiongenerator.model.Transaction;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@Log4j2
public class TransactionService {
    private final DataGenerator dataGenerator;

    public TransactionService(DataGenerator dataGenerator) {
        this.dataGenerator = dataGenerator;
    }

    public List<Transaction> getTransactions(Map<String, String> mapWithAttributes) {
        return dataGenerator.generateTransactions(mapWithAttributes);
    }
}
